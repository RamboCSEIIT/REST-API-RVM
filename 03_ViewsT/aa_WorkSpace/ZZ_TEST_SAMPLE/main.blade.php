@extends('ZZ_TEST_SAMPLE.base')

@section('title')
    Test
@endsection

@section('cssBlock')
    @include('ZZ_TEST_SAMPLE.aa_include.za_css')
@endsection

@section('content')


    <div class="  container-fluid">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                @include('ZZ_TEST_SAMPLE.aa_include.zc_navbar')
            </div>

        </div>

    </div>




    <br>
    <br>
    @include('zz_body.ZZ_TEST_SAMPLE.body')




@endsection


@section('bottomJS')

    @include('ZZ_TEST_SAMPLE.aa_include.zb_javascript')

@endsection


 
  