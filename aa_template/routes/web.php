<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



Route::get("/study","ZZ_TEST_SAMPLE\index@_zz_test_sample");

Route::resource("users","aa_user\UserController",['except'=>['create','edit']]);
Route::resource("buyers","ab_buyer\BuyerController",['only'=>['index','show']]);
Route::resource("sellers","ac_seller\SellerController",['only'=>['index','show']]);
Route::resource("transactions","ad_transaction\TransactionController",['only'=>['index','show']]);
Route::resource("categories","ae_category\CategoryController",['except'=>['create','edit']]);
Route::resource("products","af_product\ProductController",['only'=>['index','show']]);
