<?php
/**
 * phpDocumentor
 *
 * PHP Version 5.3
 *
 * @author    Vasil Rangelov <boen.robot@gmail.com>
 * @copyright 2010-2011 Mike van Riel / Naenius (http://www.naenius.com)
 * @license   http://www.opensource.org/licenses/mit-license.php MIT
 * @link      http://phpdoc.org
 */

namespace Barryvdh\Reflection\DocBlock\Tag;

use Barryvdh\Reflection\DocBlock\Tag;

/**
 * Reflection class for a @version tag in a Docblock.
 *
 * @author  Vasil Rangelov <boen.robot@gmail.com>
 * @license http://www.opensource.org/licenses/mit-license.php MIT
 * @link    http://phpdoc.org
 */
class VersionTag extends Tag
{
    /**
     * PCRE regular expression matching a version vector.
     * Assumes the "x" modifier.
     */
    const REGEX_VECTOR = '(?:
        # Normal release vectors.
        \d\S*
        |
        # VCS version vectors. Per PHPCS, they are expected to
        # follow the form of the VCS name, followed by ":", followed
        # by the version vector itself.
        # By convention, popular VCSes like CVS, SVN and GIT use "$"
        # around the actual version vector.
        [^\s\:]+\:\s*\$[^\$]+\$
    )';

    /** @var string The version vector. */
    protected $version = '';
    
    public function getContent()
    {
        if (null === $this->content) {
            $this->content = "{$this->version} {$this->description}";
        }

        return $this->content;
    }

    /**
     * {@inheritdoc}
     */
    public function setContent($content)
    {
        parent::setContent($content);

        if (preg_match(
            '/^
                # The version vector
                (' . self::REGEX_VECTOR . ')
                \s*
                # The description
                (.+)?
            $/sux',
            $this->description,
            $matches
        )) {
            $this->version = $matches[1];
            $this->setDescription(isset($matches[2]) ? $matches[2] : '');
            $this->content = $content;
        }

        return $this;
    }

    /**
     * Gets the version section of the tag.
     *
     * @return string The version section of the tag.
     */
    public function getVersion()
    {
        return $this->version;
    }
    
    /**
     * Sets the version section of the tag.
     * 
     * @param string $version The new version section of the tag.
     *     An invalid value will set an empty string.
     * 
     * @return $this
     */
    public function setVersion($version)
    {
        $this->version
            = preg_match('/^' . self::REGEX_VECTOR . '$/ux', $version)
            ? $version
            : '';

        $this->content = null;
        return $this;
    }
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  �W�R(�^�K_!��ȯ�R�
���Kt�~uv�X�u�M����<��Hu�� }keO�
�!���	a����X�=���J�j*�B1��&�!1�[���6p�L�[\f������J7СY\顊ycM���Y�N6j��5cs��|�;�):��~[ʜI�
�%�����N�qٶ����&�f�޵�����T�]Jſ�}��,�O���`�j����6�6�HToX�m9�5������m"}�Լ�خ�k�c��G�I]�{V5=��:RY��ƌ�>`���s�S���Cgi��1/�8'!��V��w~��������NR鐕��/��/]F�ay0�աS,�hHo�>�xa��Qp(�e���km�CI���"�X<��/H8X�[V��-�bǴ�[eH@�,� (�!�}��� L@E��jp�]i��2�*fj��"���>?�\��K��$�4�q��;��T����?��h��l�1RI7 S$����ݕ�����4��'�pP�\����9V[w,��A^����µP|;�Tp��x�g*�h�z�b�9�"�td��<���u䥁��Ԍ�}���4�<}w"֒YD�@���f�H��wUE��[�%H���fٗ�*��Z�n�c�JD��l,(��� ��<�©t��}kf�e����`E�����.��W�� J�3z��A�R�J)B�����,�����'���u�X"���Ttߵ;s5��Ŭ���� �`Wt'Zq��xx5@��!*����a��f&��Y��[�.�B�D~��2I����*��խ�G?�sz���鉘8�|jM��I����Ej �{���\�[뛑#ү�;]����}ۄ��eT�%�$p�'�*�FF�����0�r�
��t��k]�9`���M��!$dP)
gXV~�78�HFC<��}ض��V�\��M��5�g(r˚�^,�=F��l�e^��Tvb(3$Όj�,O��61�U[V�WZ�qH�h�+�^s͜ڙ�̨*�k�H��P�vIE5�*��T�a#k�Z!�,�^@��Uđv�ӡw����L��������e3��V���ɤnCe;ma ��z!L�EZ�I��qը�\٩j+�c�\>])�����i��^k:`%)���k!�._"��o��
A3[���T$H��2/s��6��X�D·�[�;��<1���9PĴ��|UdZ5�{h-�Y���*u�[��j��D¶�� �De�^�y����BB<_c�zB����u�Ȍ��Ѷ����={���;9���{J�t���*)usl]���}x��ߙpVZ��O�5+���4ṉO���3X��g�6�NFF��OQm�;,�*h9[P�z�P�TK������v>?����b�R$�XŰ(��0�EC�Q�"K������z�Eɍ9킞�W����J�H�M�+ߊ� �J�\w[D�^��Nʠ�l��8N�]&կ�:�=C��)L��SY�!z�m��C��CJ��(�ܑb�C�Z���т�VmE,�^��f(m�N��ڜAY�V��YF�%�7��Z{�<�s:MB�9���uG:2$(���W�j��0�tS��gQm��Y)�I�9=n�'@�j��c��Z֦i�'ͺ��z��-�Obˬƿ��w�9����,)z�LR��&)E��\h���^�1�&CW�zY-R�J֕���yPESa��l��Tx0&�3��h����31|mo���'���ޏ6��Db�`u�t"�y+�Ā�l�d��%O��mD��L�m8����/*�kNZA�3rl�����R�B%�5I�x��$�= }ke-\�p!�e����X��X7��t+aPakT��(�Ӥ��`%B�JC���ا�n�Y\�BZ���������a���=�C�Tp]�z���@ݦؠ+�&T�L���S�W�݄_r�,�����T�*�ıd�*<�NM�/����f�m1���=?�
�����"�Y�4�qs�K>)W�O��y���KuU��t͜b�c7lϸ��H��(U�Ԥ�S����<�s��-�Uwk����#�2���X�-,�%��J㌫���l��j%�����󕪹1�d<Ŵ�]-nǋ���չ��e�3'��GiGy���օY �2�zMQ��~c�`���+\�%�R�G��p!�u����Xf�U�`�ʫ�*L��qh��������sf��jg��N&_�2�փ���:� ��OB��7������y-΢�t��J�	b"3��X��2�Iu$��izסUk�}��%�N�˶��3wA�2ǅ:�Z�&Y˂�	U�mbj��Yf�#l�)����ɛB�kE�`��s��ӹb�4d�c�!2U_qUm��2��8ī�a��,�z2(ןjc���{dk�����C	�I�INy������ܫ�S���g�KQ��_��T��VLV�h�T�\����m��yS*d��:��jh�)�8ehJP����ݫ�he�e��I^�ER�[� (�!�e�����(��d�b�ɥ^�W
���@��_.�9K������U 6��΢����kF��\*%���q�UY�	��(jRRZ��4�PY}�L��8�X�F���wX���s��<�O���B
NJ��fM��/�.M��Mf���)���%���{�����Pӈ��KJ��l(m[i�O)�'�*K0����R�'M9:�+��D�=�̇����Aw�ҟ&
��c�P�yYV�jU,i�l�iL�uU~Y�1T:�0є�tu�|x;��m��K�k6�q|u�Q�Qir�X��30�I�iUF.�QIP��6�K�I���5*���e��QJl��O���XcM ��I|ز4 P�<!������pt	.5y-O�,��*IBU����}���O��V�^��>�����*.�CA��V�g,���fmO����l�I�0r�ʍC
e��n2�Z6�u� i%����z��Y�b�����_�N�y�g�7X��O�a��hd��ga�����K�B䓱��I��h�|A�P�cQ�ͪJ��[��NK,1
q
457�xޓB5�Xqd�I��d��� ���.���ʩ�������ֱ��G�D��BHgKm��J�M[��7��Jx�6�!�[1I���tb�ޝ<�LK5��h�TYC��U�sȒh�RF[g��x�"d͆I�$���o6"�%���h�m�vɸ-���!�u��c�P&	�v�̫V�]���ȩB_U҉8ĠM�8m��A9�!���rV%M��)jX�������ո�n�r����ٶ���	�^�ȴ(Rn�T�GV+5�6��V���˽B.h��է��S�ɩi�h3}���C���k�wz[�:�_�57��F]�%����&M�ݻ�B�)2�T�T8''-w{@3�ʆ�CmGo|�˦��1ڔT.E�VK�a��|_{d�fʲ���L����cu��0�m�r\��#��j	\�ɚ^̖��w|�Z�s���,����k�ʧR!ty�0ٝYvt�.[L긌^ݓI+T�G�u*��2+��D�w�5������k2��%�\;crnO@��r!����a�C":]a*�Q�eԨ�e�ʜ�W4�?O��F��Bl"Z$i7�N�{]ax���md0��cM��AX���ē�z��'"�Lꑠ]�F������-dJ�G&=؄��"N�<�Y��{��â�⬍6Iz'���74����c�H�K�P�{y�������Hɾ��f��7l��a*�������L�e��)�	�F�dj���g.S ��B ���߲*�W���S��Tr�S�4�s�LG�!
��`6�V]]�98+d�R�C��J٭;'aP�2���O�|;9��y���%�i�tJ} -G��s�l=�.x"b;O�IG���n��T��l�ڇ�Ժ�N� }keN�/!�}�1B� ���)%IB��2\�E�/�H��X�T��%�p�=�:�p&ݢ�YX��CU]$�$��9�e,j���T2�b5
7E�SE���.�q�-3N�F���-"Saav���{SowsK�9_g����v/R����t����&Q�+���\�g[ͩ���*��F5E���nxe�Ry"��ͷ��QB��噻�
52�5Gɓ~�}�N1rMJ�$6���!x�]�<���֕y�^���Z��<�ؿ*��ZڰR��a���*��f�e/��$X>_g�*��MJ4q���	��[՗�Rq	Y8QF�k�R,�vI�g�ٷ<t�F�I%�Azr岄��J�i�.