<?php

namespace App\Models\af_Product;

use App\Models\ae_category\Category;

use Illuminate\Database\Eloquent\Model;
use App\Models\ac_seller\Seller;
class Product extends Model
{
    //
    //

    const  AVAILABLE_PRODUCT ='available';
    const  UNAVAILABLE_PRODUCT ='unavailable';

     protected $fillable=[

         'name',
         'description',
         'quanity',
         'status',
         'image',
         'seller_id'

     ];

     public  function  isAvilable()
     {
         return $this->status = Product::AVAILABLE_PRODUCT;
     }



    function  categories()
    {
        return $this->belongsToMany(Category::class);
    }
    function  transaction()
    {
        return $this->hasMany(Category::class);
    }
    function  seller()
    {
        return $this->belongsTo(Seller::class);
    }
}
