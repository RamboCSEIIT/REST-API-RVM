<?php

namespace App\Models\ad_transaction;

use App\Models\ab_buyer\Buyer;
use App\Models\af_Product\Product;
use Illuminate\Database\Eloquent\Model;
class Transaction extends Model
{
    //

    protected $fillable=[


        'quanity',
        'buyer_id',
        'product_id'


    ];



    function  buyer()
    {
        return $this->belongsTo(Buyer::class);
    }
    function  products()
    {
        return $this->belongsTo(Product::class);
    }
}
