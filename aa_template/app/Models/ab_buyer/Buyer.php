<?php

namespace App\Models\ab_buyer;

use App\Models\ad_transaction\Transaction;
use App\Models\aa_user\User;


class Buyer extends User
{

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope( function($query){

          //  $query->where('type','Buyer');

        });
    }
    //
    public function  transaction()
    {
        return $this->hasMany(Transaction::class);
    }





}
