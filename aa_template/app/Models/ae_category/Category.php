<?php

namespace App\Models\ae_category;

use App\Models\af_Product\Product;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //

   protected $fillable=[
       'name',
       'description'
   ];


    function  product()
    {
        return $this->belongsToMany(Product::class);
    }
}
