<?php

namespace App\Models\ac_seller;

use App\Models\af_Product\Product;
use App\Models\aa_user\User;

class Seller extends User
{

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope( function($query){

          //  $query->where('type','Seller');

    });
    }

    public function  products()
    {
        return $this->hasMany(Product::class);
    }
}
