<?php

use Illuminate\Database\Seeder;

use App\Models\aa_user\User;
use App\Models\ae_category\Category;
use App\Models\af_Product\Product;
use App\Models\ad_transaction\Transaction;
use App\Models\ac_seller\Seller;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */


    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');

        User::truncate();
        Category::truncate();
        Product::truncate();
        Transaction::truncate();

        DB::table('category_product')->truncate();

        User::flushEventListeners();
        Category::flushEventListeners();
        Product::flushEventListeners();
        Transaction::flushEventListeners();

        $usersQuantity = 5;
        $categoriesQuantity = 5;
        $categoriesQuantity = 15;
        $productsQuantity = 5;
        $transactionsQuantity = 5;


        factory(User::class, $usersQuantity)->create();

        factory(Category::class, $categoriesQuantity)->create();

       // factory(Product::class, $productsQuantity)->create();
//        factory(Category::class, $categoriesQuantity)->create();
       // factory(Seller::class, $categoriesQuantity)->create();



               factory(Product::class, $productsQuantity)->create()->each(
                   function ($product) {


                       $categories = Category::all()->random(mt_rand(1, 5))->pluck('id');

                     // echo $categories;

                       $product->categories()->attach($categories);
                   });

               factory(Transaction::class, $transactionsQuantity)->create();




    }
}
