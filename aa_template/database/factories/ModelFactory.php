<?php

use App\Models\aa_user\User;
use App\Models\ae_category\Category;
use App\Models\af_Product\Product;
use App\Models\ad_transaction\Transaction;
use App\Models\ac_seller\Seller;
$factory->define(User::class, function (Faker\Generator $faker) {

    static $password;
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
        'verified' => $verified = $faker->randomElement([User::VERIFIED_USER, App\Models\aa_user\User::UNVERIFIED_USER]),
        'verification_token' => $verified == User::VERIFIED_USER ? null : User::generateVerificationCode(),
        'admin' => $verified = $faker->randomElement([User::ADMIN_USER, User::REGULAR_USER]),
    ];
});

 

$factory->define(App\Models\ad_transaction\Transaction::class, function (Faker\Generator $faker) {
    $seller = Seller::has('products')->get()->random();
    $buyer = User::all()->except($seller->id)->random();

    return [
        'quantity' => $faker->numberBetween(1, 3),
        'buyer_id' => $buyer->id,
        'product_id' => $seller->products->random()->id,
        // User::inRandomOrder()->first()->id
    ];
});



$factory->define(App\Models\af_Product\Product::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->word,
        'description' => $faker->paragraph(1),
        'quantity' => $faker->numberBetween(1, 10),
        'status' => $faker->randomElement([Product::AVAILABLE_PRODUCT, Product::UNAVAILABLE_PRODUCT]),
        'image' => $faker->randomElement(['1.jpg', '2.jpg', '3.jpg']),
        'seller_id' => User::all()->random()->id,
        // User::inRandomOrder()->first()->id
    ];
});

$factory->define(App\Models\ZZ_TEST_SAMPLE\Zz_dog::class, function (Faker\Generator $faker) {
    return [
    ];
});

$factory->define(App\Models\aa_user\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => bcrypt($faker->password),
        'remember_token' => str_random(10),
        'verified' => $faker->word,
        'verification_token' => $faker->word,
        'admin' => $faker->word,
        'deleted_at' => $faker->dateTimeBetween(),
    ];
});

 

$factory->define(App\Models\ab_buyer\Buyer::class, function (Faker\Generator $faker) {
    return [
    ];
});

$factory->define(App\Models\ac_seller\Seller::class, function (Faker\Generator $faker) {
    return [
    ];
});

$factory->define(App\Models\ae_category\Category::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->word,
        'description' => $faker->paragraph(1),
    ];
});

